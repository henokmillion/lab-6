#!usr/bin/python
from states import *

state = NORMAL
from handlers import *

def main():
    code = ""
    with open("/home/user/IdeaProjects/titled_testing_branch_coverage/src/com/company/Main.java") as f:
        code = f.read()
    parse(code.strip())
    print path

def parse(code):
    i = 0
    # split code in lines
    for line in code.split('\n'):
        if (line.find('{') > 0):
            if (line.find(' class ') >= 0):
                state = INCLASS
                handleClass(code[code.find(line)+len(line):], state, statement_count)
        i += 1
            
    
    


if __name__ == '__main__':
    main()
