from states import *

def handleClass(code, state, statement_count):
    i = 0
    opened = 1
    closed = 0
    for line in code.split('\n'):
        if 'if' in line:
            handleCondition(code[code.find(line) + len(line):], INCLASS, statement_count)
            break
        if '}' in line:
            closed += 1
        if closed > opened:
            return
        if ';' in line:
            statement_count += 1
            path[statement_count] = []
            try:
                if (state != INELSE):
                    path[statement_count] = [line.strip()]
                else:
                   path.pop(statement_count)
                   statement_count -= 1
                   pass
            except:
                if (state != INELSE):
                    path[statement_count].append(line.strip())
                 
        if '{' in line:
            opened += 1
        i += 1

def handleCondition(code, state, statement_count):
    i = 0
    j = 1
    k = 0
    l = 0
    opened = 1
    closed = 0
    condition = NORMAL
    for line in code.split('\n'):
        i += 1
        if line.find('{') >=0:
            opened += 1 
        if line.find(';') >= 0:
            if (condition == INELSE):
                path[statement_count][k][str(statement_count)+'(F)'].append({l: line.strip()})
                l += 1
                continue
            try:
                path[statement_count][k][str(statement_count)+'(T)'].append({j: line.strip()})
            except:
                if ({str(statement_count) + '(T)'} not in path[statement_count]):
                    path[statement_count].append({str(statement_count)+'(T)': [{j: line.strip()}]})
                    k += 1
                    j += 1
        if line.find('}') >= 0:
            closed += 1
        if line.find('else') >= 0:
            path[statement_count].append({str(statement_count)+'(F)':[]})
            condition = INELSE
            l += 1
            k += 1
            continue
        if closed >= opened:
            handleClass(code[code.find(line) + len(line):], condition, statement_count)
            break
            condition = NORMAL
